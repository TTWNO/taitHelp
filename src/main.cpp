/* +----------------------------------------------------------------------+
 * | Developer: Justin Pilon                                              |
 * | C++                                                                  |
 * |                                                                      |
 * | Compiles with: g++ -o main2 main2.cpp -lwiringPi -pthread -std=c++11 |
 * +----------------------------------------------------------------------+
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <iostream>
#include <wiringPi.h>
#include <wiringSerial.h>
#include <termios.h>
#include <string>
#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>

using namespace std;

std::mutex mtx;
std::condition_variable cv;
bool ready = false;
int current = 0;

int fd = 0;
char j[100];

void run()
{
  std::unique_lock<std::mutex> lck(mtx);
  ready = true;
  cv.notify_all();
}

// You dfined the fuction ad int fd, char* j above, and then didn't repeat the process here.
// Instead, you just put df, j...which doesn't work as you *need* to declare types in c++
// You don't need to return anything here, just make it void.
void toTermite(int num, int fd, char* j)
{
  while(1)
  {
    std::unique_lock<std::mutex> lck(mtx);
    while(num != current || !ready){ cv.wait(lck); }
    //current++;
    // To termite
    cout << "Send ("<<num<<"), Count="<<current<<" -> ";
    cin >> j;
    // Run serialPrint, just don't return whatever it's returning.
    serialPrintf(fd, j);
    cv.notify_all();
  }
}

void fromTermite(int num, int fd){
  while(1)
  {
    std::unique_lock<std::mutex> lck(mtx);
    while(num != current || !ready){ cv.wait(lck); }
    //current++;
    // From termite
    while (serialDataAvail(fd))
    {
      char ascii;
      cout << (char)serialGetchar(fd) << " <- Recieved ("<<num<<"), Count="<<current;
      fflush (stdout) ;
    }
    printf("\n");
    // Can't return 0 in a void method.
    //return 0;
    cv.notify_all();
  }
}

int main ()
{
  int threadnum = 2;
  std::thread threads[2];

  if ((fd = serialOpen ("/dev/ttyUSB0", 115200)) < 0)
  {
    fprintf (stderr, "[ Unable to open serial device ] =: %s\n", strerror (errno)) ;
    return 1 ;
  }

  if (wiringPiSetup () == -1)
  {
    fprintf (stdout, "[ Unable to start wiringPi ] =: %s\n", strerror (errno)) ;
    return 1 ;
  }

  threads[0] = std::thread(toTermite ,0 ,fd ,j);
  threads[1] = std::thread(fromTermite ,1 ,fd);

  std::cout <<"\nRunning " << threadnum;
  std::cout <<" in parallel: \n" << std::endl;

  run();

  for (int id = 0; id < threadnum; id++)
  {
    threads[id].join();
  }

  return 0;
}
