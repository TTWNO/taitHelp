default: build

EXTRA_LIBS=lib/WiringPi/wiringPi/
COMPILER=g++

build:
	test -d bin || mkdir bin
	$(COMPILER) src/main.cpp -o bin/main.run -I$(EXTRA_LIBS) -pthread --std=c++11
clean:
	rm -r bin
